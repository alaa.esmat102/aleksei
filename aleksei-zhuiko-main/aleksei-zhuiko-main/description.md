### Prerequisites

- Java 11 or above required

### How to build

For building use the next command:
`mvn clean package`
I use dependencies from Maven Central

### How to start

For starting use next command:
`java -jar .\target\drones-1.0.0.jar`

NOTE: Please make sure that Java is available in your environment.  
You can check it by the next command:
`java -version`

### Swagger

I used swagger annotations, so you can test application by using
it. [Link to Swagger UI](http://localhost:8080/swagger-ui.html)

### Comments

#### Entity

I prefer to write table names explicitly because it is simpler to manage SQL-scripts with database
migrations. So, you can compare your entity with tables and constraints.

#### Tests

I wrote some tests, for demonstrating my skills, they are not test everything.
Also, I didn't write any integration tests (except
ru.avzhuiko.drones.repository.DroneRepositoryTest), because it takes a lot of time.