INSERT INTO medication (id, name, weight, code, image, mime_type, filename)
VALUES (100, 'medication 1', 342, 'MED_CODE_100', FILE_READ('classpath:dummy-icon.png'),
        'image/png', 'dummy-icon.png'),
       (110, 'medication 2', 300, 'MED_CODE_110', null, null, null);

INSERT INTO drone (id, battery_capacity, model, serial_number, state, weight_limit)
VALUES (1000, 56, 'MIDDLEWEIGHT', 'sn_1000', 'IDLE', 500);

INSERT INTO drone_medications (drone_id, medication_id)
VALUES (1000, 100),
       (1000, 110);