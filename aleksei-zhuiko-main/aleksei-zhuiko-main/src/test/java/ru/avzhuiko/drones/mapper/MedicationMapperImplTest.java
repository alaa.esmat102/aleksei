package ru.avzhuiko.drones.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.nio.file.Files;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;
import ru.avzhuiko.drones.entity.Image;
import ru.avzhuiko.drones.entity.Medication;

@ExtendWith(MockitoExtension.class)
public class MedicationMapperImplTest {

  @InjectMocks
  private MedicationMapperImpl mapper;

  @Test
  @DisplayName("Mapping with all filled fields")
  public void convert_entityToDto() throws Exception {
    var image = ResourceUtils.getFile("classpath:dummy-icon.png");
    var entity = new Medication();
    entity.setId(RandomUtils.nextLong());
    entity.setName(RandomStringUtils.randomAlphanumeric(30));
    entity.setCode(RandomStringUtils.randomAlphanumeric(30).toUpperCase());
    entity.setWeight(RandomUtils.nextInt(0, 500));
    entity.setImage(new Image(
        "dummy-icon.png",
        "image/png",
        Files.readAllBytes(image.toPath())));

    var result = mapper.convert(entity);

    assertNotNull(result);
    assertEquals(entity.getId(), result.getId());
    assertEquals(entity.getName(), result.getName());
    assertEquals(entity.getCode(), result.getCode());
    assertEquals(entity.getWeight(), result.getWeight());

    var encodedImage = ResourceUtils.getFile("classpath:dummy-icon.base64");
    var resultImage = result.getImage();
    assertNotNull(resultImage);
    assertEquals(entity.getImage().getFilename(), resultImage.getFilename());
    assertEquals(entity.getImage().getMimeType(), resultImage.getMimeType());
    assertEquals(Files.readString(encodedImage.toPath()), resultImage.getEncodedImage());
  }

}