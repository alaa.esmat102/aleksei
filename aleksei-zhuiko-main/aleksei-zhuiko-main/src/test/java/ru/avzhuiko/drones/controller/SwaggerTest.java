package ru.avzhuiko.drones.controller;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@ImportAutoConfiguration({AopAutoConfiguration.class})
@Disabled("Use it if you want to check swagger config/annotations")
public class SwaggerTest {

  @Autowired
  protected MockMvc mockMvc;

  @Test
  @DisplayName("Debug swagger description")
  public void swaggerDebug() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/v3/api-docs.yaml"))
        .andExpect(status().isOk())
        .andReturn();
  }

}