package ru.avzhuiko.drones.controller.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import javax.annotation.Nonnull;
import javax.validation.Validator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import ru.avzhuiko.drones.api.dto.MedicationDto;

@SpringBootTest(
    classes = {
        ValidationAutoConfiguration.class
    }
)
public class MedicationValidationTest {

  @Autowired
  private Validator validator;

  @Test
  public void isValid() {
    var dto = buildGoodMedicationDto();

    var violations = validator.validate(dto);

    assertThat(violations, hasSize(0));
  }

  @Nonnull
  private MedicationDto buildGoodMedicationDto() {
    var dto = new MedicationDto();
    dto.setId(100L);
    dto.setName("Medication_100");
    dto.setWeight(100);
    dto.setCode("MEDICATION_CODE_100");
    return dto;
  }

}
