package ru.avzhuiko.drones.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Files;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import ru.avzhuiko.drones.api.dto.DroneModel;
import ru.avzhuiko.drones.api.dto.DroneState;

public class DroneRepositoryTest extends BaseJpaTest {

  @Autowired
  private DroneRepository repository;

  @Value("dummy-icon.png")
  private Resource image;

  @Test
  @Sql("classpath:/db/drone-repository-test.sql")
  public void readOne() throws Exception {
    var result = repository.getReferenceById(1000L);
    assertNotNull(result);
    assertEquals(1000L, result.getId());
    assertEquals("sn_1000", result.getSerialNumber());
    assertEquals(DroneState.IDLE, result.getState());
    assertEquals(DroneModel.MIDDLEWEIGHT, result.getModel());
    assertEquals(56, result.getBatteryCapacity());
    assertThat(result.getMedications(), hasSize(2));

    var optMedication1 = result.getMedications().stream()
        .filter(medication -> 100L == medication.getId()).findFirst();
    assertTrue(optMedication1.isPresent());
    var medication1 = optMedication1.get();
    assertEquals("MED_CODE_100", medication1.getCode());
    assertEquals(342, medication1.getWeight());
    assertEquals("medication 1", medication1.getName());

    assertNotNull(medication1.getImage());
    var medication1Image = medication1.getImage();
    assertArrayEquals(Files.readAllBytes(image.getFile().toPath()), medication1Image.getImage());
    assertEquals("dummy-icon.png", medication1Image.getFilename());
    assertEquals("image/png", medication1Image.getMimeType());

    var optMedication2 = result.getMedications().stream()
        .filter(medication -> 110L == medication.getId()).findFirst();
    assertTrue(optMedication2.isPresent());
    var medication2 = optMedication2.get();
    assertEquals("MED_CODE_110", medication2.getCode());
    assertEquals(300, medication2.getWeight());
    assertEquals("medication 2", medication2.getName());
    assertNull(medication2.getImage());
  }

}