package ru.avzhuiko.drones.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;

import java.util.List;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.avzhuiko.drones.api.dto.DroneModel;
import ru.avzhuiko.drones.api.dto.DroneState;
import ru.avzhuiko.drones.entity.Drone;
import ru.avzhuiko.drones.entity.Medication;

@ExtendWith(MockitoExtension.class)
public class DroneMapperImplTest {

  @Mock
  private MedicationMapper medicationMapper;

  @InjectMocks
  private DroneMapperImpl mapper;

  @Test
  @DisplayName("Mapping with all filled fields")
  public void convert_entityToDto() throws Exception {
    var medication1 = new Medication();
    medication1.setId(RandomUtils.nextLong());
    var medication2 = new Medication();
    medication2.setId(RandomUtils.nextLong());

    var entity = new Drone();
    entity.setId(RandomUtils.nextLong());
    entity.setState(DroneState.LOADED);
    entity.setModel(DroneModel.LIGHTWEIGHT);
    entity.setBatteryCapacity(RandomUtils.nextInt(0, 100));
    entity.setWeightLimit(RandomUtils.nextInt(0, 500));
    entity.setMedications(List.of(medication1, medication2));

    var result = mapper.convert(entity);

    assertNotNull(result);
    assertEquals(entity.getId(), result.getId());
    assertEquals(entity.getState(), result.getState());
    assertEquals(entity.getModel(), result.getModel());
    assertEquals(entity.getBatteryCapacity(), result.getBatteryCapacity());
    assertEquals(entity.getWeightLimit(), result.getWeightLimit());
    verify(medicationMapper).convert(medication1);
    verify(medicationMapper).convert(medication2);
    assertThat(entity.getMedications(), hasSize(2));
  }

}