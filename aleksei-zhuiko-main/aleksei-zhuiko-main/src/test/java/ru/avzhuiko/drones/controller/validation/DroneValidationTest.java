package ru.avzhuiko.drones.controller.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import javax.annotation.Nonnull;
import javax.validation.Validator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.DroneModel;

@SpringBootTest(
    classes = {
        ValidationAutoConfiguration.class
    }
)
public class DroneValidationTest {

  @Autowired
  private Validator validator;

//  @Test
  public void isValid() {
    var dto = buildGoodDroneDto();

    var violations = validator.validate(dto);

    assertThat(violations, hasSize(0));
  }

  @Nonnull
  private DroneDto buildGoodDroneDto() {
    var dto = new DroneDto();
    dto.setId(1000L);
    dto.setSerialNumber("serNum");
    dto.setModel(DroneModel.LIGHTWEIGHT);
    dto.setWeightLimit(489);
    return dto;
  }

}
