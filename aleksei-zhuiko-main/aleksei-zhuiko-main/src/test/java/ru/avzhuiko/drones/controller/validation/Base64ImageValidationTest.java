package ru.avzhuiko.drones.controller.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.nio.file.Files;
import javax.annotation.Nonnull;
import javax.validation.Validator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;
import ru.avzhuiko.drones.api.dto.Base64ImageDto;

@SpringBootTest(
    classes = {
        ValidationAutoConfiguration.class
    }
)
public class Base64ImageValidationTest {

  @Autowired
  private Validator validator;

  @Test
  public void isValid() throws Exception {
    var dto = buildGoodBase64ImageDto();

    var violations = validator.validate(dto);

    assertThat(violations, hasSize(0));
  }

  @Nonnull
  private Base64ImageDto buildGoodBase64ImageDto() throws Exception {
    var encodedImage = ResourceUtils.getFile("classpath:dummy-icon.base64");
    var dto = new Base64ImageDto();
    dto.setFilename("filename.jpg");
    dto.setMimeType("image/jpeg");
    dto.setEncodedImage(Files.readString(encodedImage.toPath()));
    return dto;
  }

}
