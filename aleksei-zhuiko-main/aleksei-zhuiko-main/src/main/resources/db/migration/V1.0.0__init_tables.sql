create sequence drone_seq start with 1 increment by 10;
create sequence medication_seq start with 1 increment by 10;
create table drone
(
    id               bigint default nextVal('drone_seq') not null,
    battery_capacity integer                             not null,
    model            varchar(255)                        not null,
    serial_number    varchar(100)                        not null,
    state            varchar(255)                        not null,
    weight_limit     integer                             not null,
    primary key (id)
);

create table medication
(
    id        bigint default nextVal('medication_seq') not null,
    code      varchar(255)                             not null,
    filename  varchar(255),
    image     blob,
    mime_type varchar(255),
    name      varchar(255)                             not null,
    weight    integer                                  not null,
    primary key (id)
);

create table drone_medications
(
    drone_id      bigint not null,
    medication_id bigint not null
);

alter table drone_medications
    add constraint uk_drone_medications_medication_id unique (medication_id);
alter table drone_medications
    add constraint fk_drone_medications_medication_id foreign key (medication_id) references medication;
alter table drone_medications
    add constraint fk_drone_medications_drone_id foreign key (drone_id) references drone;