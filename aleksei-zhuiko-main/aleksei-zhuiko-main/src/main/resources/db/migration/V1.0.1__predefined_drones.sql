INSERT INTO drone (battery_capacity, model, serial_number, state, weight_limit)
VALUES (100, 'LIGHTWEIGHT', 'sn_1', 'IDLE', 100),
       (100, 'LIGHTWEIGHT', 'sn_2', 'IDLE', 100),
       (100, 'MIDDLEWEIGHT', 'sn_3', 'IDLE', 200),
       (100, 'MIDDLEWEIGHT', 'sn_4', 'IDLE', 200),
       (100, 'CRUISERWEIGHT', 'sn_5', 'IDLE', 300),
       (100, 'CRUISERWEIGHT', 'sn_6', 'IDLE', 300),
       (100, 'CRUISERWEIGHT', 'sn_7', 'IDLE', 300),
       (100, 'HEAVYWEIGHT', 'sn_8', 'IDLE', 500),
       (100, 'HEAVYWEIGHT', 'sn_9', 'IDLE', 500),
       (100, 'HEAVYWEIGHT', 'sn_10', 'IDLE', 500);