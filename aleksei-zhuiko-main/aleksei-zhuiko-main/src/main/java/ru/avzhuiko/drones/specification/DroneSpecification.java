package ru.avzhuiko.drones.specification;

import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;
import ru.avzhuiko.drones.api.dto.filter.DroneFilter;
import ru.avzhuiko.drones.entity.Drone;
import ru.avzhuiko.drones.entity.Drone.Fields;

@RequiredArgsConstructor
public class DroneSpecification implements Specification<Drone> {

  private final DroneFilter filter;

  @Override
  public Predicate toPredicate(
      @Nonnull Root<Drone> root,
      @Nonnull CriteriaQuery<?> criteriaQuery,
      @Nonnull CriteriaBuilder cb
  ) {
    if (filter == null) {
      return null;
    }
    Predicate predicate = cb.and();
    if (!CollectionUtils.isEmpty(filter.getStates())) {
      var idExpression = root.get(Fields.state)
          .in(filter.getStates());
      predicate = cb.and(predicate, idExpression);
    }

    return predicate;
  }

}