package ru.avzhuiko.drones.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "medication")
public class Medication {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medication_generator")
  @SequenceGenerator(
      name = "medication_generator",
      sequenceName = "medication_seq",
      allocationSize = 10
  )
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private int weight;

  @Column(nullable = false)
  private String code;

  // Here we can replace it with link to any kind of storage, e.g. Amazon S3
  @Embedded
  private Image image;

}
