package ru.avzhuiko.drones.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import ru.avzhuiko.drones.api.dto.DroneModel;
import ru.avzhuiko.drones.api.dto.DroneState;

@Data
@NoArgsConstructor
@FieldNameConstants
@Entity
@Table(name = "drone")
public class Drone {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drone_generator")
  @SequenceGenerator(
      name = "drone_generator",
      sequenceName = "drone_seq",
      allocationSize = 10
  )
  private Long id;

  @Column(nullable = false, length = 100)
  private String serialNumber;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private DroneModel model;

  @Column(nullable = false)
  private int weightLimit;

  @Column(nullable = false)
  private int batteryCapacity;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private DroneState state;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "drone_medications",
      uniqueConstraints = @UniqueConstraint(name = "uk_drone_medications_medication_id", columnNames = "medication_id"),
      joinColumns = @JoinColumn(name = "drone_id"),
      inverseJoinColumns = @JoinColumn(name = "medication_id"),
      foreignKey = @ForeignKey(name = "fk_drone_medications_drone_id"),
      inverseForeignKey = @ForeignKey(name = "fk_drone_medications_medication_id"))
  private List<Medication> medications = new ArrayList<>();

}
