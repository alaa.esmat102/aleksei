package ru.avzhuiko.drones.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springdoc.api.annotations.ParameterObject;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.filter.DroneFilter;

@Tag(
    name = "drone",
    description = "Resource for working with drones"
)
@RequestMapping("/drones")
public interface DroneController {

  @Operation(
      summary = "Endpoint for filtering drones"
  )
  @ApiResponses({
      @ApiResponse(description = "Operation was successful"),
  })
  @PageableAsQueryParam
  @GetMapping
  Page<DroneDto> findAll(
      @ParameterObject @Nullable DroneFilter filter,
      @Parameter(hidden = true) @Nonnull Pageable pageable
  );

  @Operation(
      summary = "Endpoint for getting information about specific drone"
  )
  @ApiResponses({
      @ApiResponse(description = "Operation was successful"),
      @ApiResponse(responseCode = "404", description = "Drone not found"),
  })
  @GetMapping("/{id}")
  DroneDto get(@PathVariable("id") long id);

  @Operation(
      summary = "Endpoint for drone creation"
  )
  @ApiResponses({
      @ApiResponse(description = "Operation was successful"),
      @ApiResponse(responseCode = "400", description = "Dto has validation errors"),
  })
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  DroneDto create(@RequestBody @Validated @Nonnull DroneDto dto);

  @Operation(
      summary = "Endpoint for drone updating"
  )
  @ApiResponses({
      @ApiResponse(description = "Operation was successful"),
      @ApiResponse(responseCode = "400", description = "Dto has validation errors"),
  })
  @PatchMapping
  DroneDto update(@RequestBody @Validated @Nonnull DroneDto dto);

}
