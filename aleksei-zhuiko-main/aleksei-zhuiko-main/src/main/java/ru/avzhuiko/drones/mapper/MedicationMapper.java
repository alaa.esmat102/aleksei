package ru.avzhuiko.drones.mapper;

import java.util.Base64;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.avzhuiko.drones.api.dto.MedicationDto;
import ru.avzhuiko.drones.entity.Medication;

@Mapper
public interface MedicationMapper {

  @Nullable
  @Mapping(target = "image.encodedImage", source = "image.image")
  MedicationDto convert(@Nullable Medication entity);

  @Nullable
  @Mapping(target = "image.image", source = "image.encodedImage")
  Medication convert(@Nullable MedicationDto dto);

  default String bytesToBase64(@Nonnull byte[] bytes) {
    return Base64.getEncoder().encodeToString(bytes);
  }

  default byte[] base64ToByte(@Nonnull String encoded) {
    return Base64.getDecoder().decode(encoded);
  }

}
