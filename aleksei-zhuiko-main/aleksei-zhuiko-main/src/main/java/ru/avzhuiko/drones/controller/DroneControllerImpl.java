package ru.avzhuiko.drones.controller;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestController;
import ru.avzhuiko.drones.api.controller.DroneController;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.filter.DroneFilter;
import ru.avzhuiko.drones.facade.DroneFacade;
import ru.avzhuiko.drones.validation.DroneDtoValidator;

@RestController
public class DroneControllerImpl implements DroneController {

  @Autowired
  private DroneFacade facade;

  @Autowired
  private DroneDtoValidator droneDtoValidator;

  @InitBinder
  public void initBinder(WebDataBinder binder) {

//    binder.addValidators(droneDtoValidator);
  }

  @Override
  public Page<DroneDto> findAll(@Nullable DroneFilter filter, @Nonnull Pageable pageable) {
    return facade.findAll(filter, pageable);
  }

  @Override
  public DroneDto get(long id) {
    return facade.get(id);
  }

  @Override
  public DroneDto create(@Nonnull DroneDto dto) {
    return facade.create(dto);
  }

  @Override
  public DroneDto update(@Nonnull DroneDto dto) {
    return facade.update(dto);
  }

}
