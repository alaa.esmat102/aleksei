package ru.avzhuiko.drones.facade.impl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.filter.DroneFilter;
import ru.avzhuiko.drones.facade.DroneFacade;
import ru.avzhuiko.drones.mapper.DroneMapper;
import ru.avzhuiko.drones.service.DroneService;
import ru.avzhuiko.drones.specification.DroneSpecification;

@Component
@RequiredArgsConstructor
public class DroneFacadeImpl implements DroneFacade {

  private final DroneMapper mapper;
  private final DroneService service;

  @Override
  @Transactional(readOnly = true)
  public Page<DroneDto> findAll(@Nullable DroneFilter filter, @Nonnull Pageable pageable) {
    var spec = new DroneSpecification(filter);
    return service.findAll(spec, pageable)
        .map(mapper::convert);
  }

  @Override
  @Transactional(readOnly = true)
  public DroneDto get(long id) {
    return service.get(id)
        .map(mapper::convert)
        .orElseThrow(() -> new EntityNotFoundException(
            String.format("Drone with id=%s is not found", id)));
  }

  @Override
  @Transactional
  public DroneDto create(@Nonnull DroneDto dto) {
    return mapper.convert(service.save(mapper.convert(dto)));
  }

  @Override
  @Transactional
  public DroneDto update(@Nonnull DroneDto dto) {
    return mapper.convert(service.save(mapper.convert(dto)));
  }

}
