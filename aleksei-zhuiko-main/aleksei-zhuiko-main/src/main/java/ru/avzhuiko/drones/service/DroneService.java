package ru.avzhuiko.drones.service;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.avzhuiko.drones.entity.Drone;

/**
 * Service for interaction with drone entities.
 */
public interface DroneService {

  /**
   * Retrieve page of drones by specified filter.
   *
   * @param specification specification for filtration
   * @param pageable      paging parameters must not be {@literal null}
   * @return page with filtered drones
   */
  Page<Drone> findAll(@Nullable Specification<Drone> specification, @Nonnull Pageable pageable);

  /**
   * Fetch drone.
   *
   * @param id drone id
   * @return information about drone
   */
  Optional<Drone> get(long id);

  /**
   * Save drone into repository.
   *
   * @param entity for saving
   * @return saved entity
   */
  Drone save(@Nonnull Drone entity);

}
