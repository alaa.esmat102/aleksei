package ru.avzhuiko.drones.api.dto.filter;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import ru.avzhuiko.drones.api.dto.DroneState;

@Data
public class DroneFilter {

  @Parameter(
      name = "states",
      description = "Array of drone states",
      in = ParameterIn.QUERY,
      example = "IDLE",
      array = @ArraySchema(schema = @Schema(implementation = DroneState.class)))
  private Set<DroneState> states = new HashSet<>();

}
