package ru.avzhuiko.drones.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.DroneState;
import ru.avzhuiko.drones.api.dto.MedicationDto;

@Component
public class DroneDtoValidator implements Validator {

  @Override
  public boolean supports(Class<?> clazz) {
    return DroneDto.class.equals(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {
    var dto = (DroneDto) target;
    checkMedications(dto, errors);
    checkBatteryCapacity(dto, errors);
  }

  /**
   * Check that medications total weight is not more than weight limit of drone.
   */
  private void checkMedications(DroneDto dto, Errors errors) {
    var totalMedicationsWeight = dto.getMedications().stream()
        .map(MedicationDto::getWeight)
        .reduce(0, Integer::sum);
    var weightLimit = dto.getWeightLimit();
    if (totalMedicationsWeight > weightLimit) {
      errors.rejectValue("medications", "medications.overweight");
    }
  }

  /**
   * Check that medications total weight do not more than weight limit of drone.
   */
  private void checkBatteryCapacity(DroneDto dto, Errors errors) {
    var state = dto.getState();
    var batteryCapacity = dto.getBatteryCapacity();

    if (state == DroneState.LOADING && batteryCapacity < 25) {
      errors.rejectValue("batteryCapacity", "battery.lower.than.25");
      errors.rejectValue("state", "battery.lower.than.25");
    }
  }

}
