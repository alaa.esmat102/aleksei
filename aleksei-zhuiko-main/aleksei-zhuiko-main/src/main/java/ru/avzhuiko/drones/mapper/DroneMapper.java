package ru.avzhuiko.drones.mapper;

import javax.annotation.Nonnull;
import org.mapstruct.Mapper;
import org.mapstruct.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.entity.Drone;
import ru.avzhuiko.drones.repository.DroneRepository;

@Mapper(
    uses = {
        MedicationMapper.class,
    }
)
public abstract class DroneMapper {

  @Autowired
  private DroneRepository droneRepository;

  public abstract DroneDto convert(Drone entity);

  public abstract Drone convert(DroneDto dto);

  @ObjectFactory
  protected Drone build(@Nonnull DroneDto dto) {
    if (dto.getId() == null) {
      return new Drone();
    }
    return droneRepository.getReferenceById(dto.getId());
  }

}
