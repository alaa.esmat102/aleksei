package ru.avzhuiko.drones.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
public class Base64ImageDto {

  @NotBlank
  @Pattern(regexp = "[A-Za-z0-9+/]*={0,2}", message = "{encodedImage.pattern.error}")
  private String encodedImage;

  @NotBlank
  private String mimeType;

  @NotBlank
  private String filename;

}