package ru.avzhuiko.drones.api.dto;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class DroneDto {

  private Long id;

  @NotBlank
  @Size(max = 100)
  private String serialNumber;

  @NotNull
  private DroneModel model;

  @NotNull
  @Min(value = 0)
  @Max(value = 500)
  private int weightLimit;

  @Min(value = 0)
  @Max(value = 100)
  private int batteryCapacity;

  @NotNull
  private DroneState state;

  @Valid
  private List<MedicationDto> medications = new ArrayList<>();

}
