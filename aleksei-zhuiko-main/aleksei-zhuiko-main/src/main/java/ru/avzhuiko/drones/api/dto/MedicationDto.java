package ru.avzhuiko.drones.api.dto;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
public class MedicationDto {

  private Long id;

  @NotBlank
  @Pattern(regexp = "[\\p{L}|0-9_\\-]+", message = "{medication.name.pattern.error}")
  private String name;

  @NotNull
  @Min(value = 0)
  private int weight;

  @NotBlank
  @Pattern(regexp = "[\\p{Lu}|0-9_]+", message = "{medication.code.pattern.error}")
  private String code;

  @Nullable
  @Valid
  private Base64ImageDto image;

}
