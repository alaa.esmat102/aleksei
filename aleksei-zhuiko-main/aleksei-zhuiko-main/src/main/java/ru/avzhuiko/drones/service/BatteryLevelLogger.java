package ru.avzhuiko.drones.service;


import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.avzhuiko.drones.repository.DroneRepository;

@Component
@RequiredArgsConstructor
@Slf4j
public class BatteryLevelLogger {

  private final DroneRepository droneRepository;

  @Scheduled(fixedRate = 10000L)
  @Transactional(readOnly = true)
  public void log() {
    var message = droneRepository.findAll().stream()
        .map(drone -> String.format("Battery capacity of drone with id=%s is %s%%",
            drone.getId(),
            drone.getBatteryCapacity()))
        .collect(Collectors.joining("\n"));
    log.info(message);
  }

}
