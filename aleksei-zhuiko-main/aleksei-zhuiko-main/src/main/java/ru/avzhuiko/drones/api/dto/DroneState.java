package ru.avzhuiko.drones.api.dto;

public enum DroneState {

  IDLE,
  LOADING,
  LOADED,
  DELIVERING,
  DELIVERED,
  RETURNING,

}
