package ru.avzhuiko.drones.service.impl;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.avzhuiko.drones.entity.Drone;
import ru.avzhuiko.drones.repository.DroneRepository;
import ru.avzhuiko.drones.service.DroneService;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {

  private final DroneRepository repository;

  @Override
  public Page<Drone> findAll(
      @Nullable Specification<Drone> specification,
      @Nonnull Pageable pageable
  ) {
    return repository.findAll(specification, pageable);
  }

  @Override
  public Optional<Drone> get(long id) {
    return repository.findById(id);
  }

  @Override
  public Drone save(@Nonnull Drone entity) {
    return repository.save(entity);
  }

}
