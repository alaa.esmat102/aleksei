package ru.avzhuiko.drones.api.dto;

public enum DroneModel {

  LIGHTWEIGHT,

  MIDDLEWEIGHT,

  CRUISERWEIGHT,

  HEAVYWEIGHT,

}
