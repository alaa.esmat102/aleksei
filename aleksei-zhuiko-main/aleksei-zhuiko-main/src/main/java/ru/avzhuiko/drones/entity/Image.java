package ru.avzhuiko.drones.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Image {

  @Column(nullable = false)
  private String filename;

  @Column(nullable = false)
  private String mimeType;

  @Lob
  @Column(nullable = false)
  private byte[] image;


}
