package ru.avzhuiko.drones.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.avzhuiko.drones.entity.Drone;

public interface DroneRepository extends JpaRepository<Drone, Long>,
    JpaSpecificationExecutor<Drone> {

}
