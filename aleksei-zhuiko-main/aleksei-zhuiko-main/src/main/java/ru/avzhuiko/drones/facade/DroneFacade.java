package ru.avzhuiko.drones.facade;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.avzhuiko.drones.api.dto.DroneDto;
import ru.avzhuiko.drones.api.dto.filter.DroneFilter;

/**
 * Facade for interaction with drones.
 */
public interface DroneFacade {

  /**
   * Retrieve page of drones by specified filter.
   *
   * @param filter   filtration parameters
   * @param pageable paging parameters must not be {@literal null}
   * @return page with filtered drones
   */
  Page<DroneDto> findAll(@Nullable DroneFilter filter, @Nonnull Pageable pageable);

  /**
   * Fetch drone.
   *
   * @param id drone id
   * @return information about drone
   */
  DroneDto get(long id);

  /**
   * Create drone.
   *
   * @param dto information about drone
   * @return saved drone
   */
  DroneDto create(@Nonnull DroneDto dto);

  /**
   * Update drone.
   *
   * @param dto information about drone
   * @return saved drone
   */
  DroneDto update(@Nonnull DroneDto dto);

}
