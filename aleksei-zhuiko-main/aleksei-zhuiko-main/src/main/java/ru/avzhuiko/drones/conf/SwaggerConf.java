package ru.avzhuiko.drones.conf;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
    info = @Info(
        title = "Drones",
        version = "1.0.0",
        contact = @Contact(
            name = "Aleksei Zhuiko",
            email = "avzhuiko@gmail.com"
        )
    )
)
@Configuration
public class SwaggerConf {

}
